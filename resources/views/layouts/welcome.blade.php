<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Nextoran') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/all.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('css/pagination.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

      <style>
        .kosong{
          margin-top: 50px;
        }
      </style>
</head>
<body>
    <div id="app">
      <nav class="navbar navbar-expand-xl fixed-top bg-nextoran navbar-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">{{ config('app.name', 'Laravel') }}</a>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
          <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item"><a class="nav-link" href="{{ url('/') }}">Home</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('/menu') }}">Menu</a></li>
            <li class="nav-item">
              <a class="nav-link" href="#" data-toggle="modal" data-target="#modalOrder">
                Order
              </a>
            </li>
          </ul>
        </div>
      </nav>

        <!-- Modal -->
        <div class="modal fade" id="modalOrder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="/order_pelanggan" method="POST">
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" name="nama" placeholder="Masukan nama anda" required>
                  </div>
                  <div class="form-group">
                    <label for="id_meja">Meja</label>
                    <select class="form-control" name="id_meja" required>
                      @foreach ($meja as $mejas)
                        <option value="{{ $mejas->id }}">{{ $mejas->nama_meja }}</option>
                      @endforeach
                    </select>
                  </div>
                {{ csrf_field() }}
                <button type="submit" name="order" class="btn btn-outline-primary btn-block"> Order </button>
              </form>
              </div>
            </div>
          </div>
        </div>

      {{-- content --}}
        @yield('content')

        </div>
            {{-- footer --}}
            <footer id="footer">
                  {{--  Author : Firan
                  TM @firstioanmar_  --}}
                    Copyright &copy; 2019 {{ config('app.name', 'Laravel') }}. All Right Reserved
            </footer>

            <button id="scrollTopBtn">
              <i class="fas fa-arrow-up"></i>
            </button>

            <!-- Scripts -->
            <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
            <script src="{{ asset('js/popper.min.js') }}"></script>
            <script src="{{ asset('js/bootstrap.min.js') }}"></script>

            <script type="text/javascript">
            $(document).ready(function(){

              $('#scrollTopBtn').click(function(){
                $('html ,body').animate({scrollTop : 0},800);
              })


           });
            </script>

</body>
</html>
