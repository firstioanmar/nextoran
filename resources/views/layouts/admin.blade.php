<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Nextoran') }}</title>

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="{{ asset('css/all.css') }}" rel="stylesheet">
  <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet">
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">

  {{-- data table --}}
  <script type="text/javascript" src="{{ asset('datatables/datatables.min.js')}}"></script>
  {{-- <link rel="stylesheet" href="{{ asset('css/jquery.dataTable.min.css') }}"> --}}

</head>
<body>
  <div id="app">
    <nav id="supernav" class="navbar navbar-dark sticky-top bg-nextoran flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">{{ config('app.name', 'Laravel') }}</a>
      <input class="form-control form-control-dark w-100" style="opacity:0;" type="text" aria-label="Search" readonly>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="{{ route('logout') }}"
          onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">
          Logout
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
        </form>
      </li>
    </ul>
  </nav>

  <div class="container-fluid">
    <div class="row">
      <nav class="col-md-2 d-none d-md-block bg-light sidebar">
        <div class="sidebar-sticky">
          <ul class="nav flex-column">
            <li class="nav-item">
              <a class="nav-link" href="/admin">
                Dashboard <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/orders">
                Orders
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/masakan">
                Masakan
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/users">
                User
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                Laporan
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/meja">
                Meja
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/level">
                Level
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/carousel">
                Carousel
              </a>
            </li>
            <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Change Color
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a id="warna1" class="dropdown-item" href="#"><img style="background: #FF6699" src="..." alt="" class="rounded-circle">Pink</a>
          <a id="warna2" class="dropdown-item" href="#">Ijo</a>
          <a id="warna3" class="dropdown-item" href="#">Biru</a>
        </li>
        </div>
          </ul>

          <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>Saved reports</span>
            <a class="d-flex align-items-center text-muted" href="#">
              <span data-feather="plus-circle"></span>
            </a>
          </h6>
          <ul class="nav flex-column mb-2">
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Current month
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Last quarter
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Social engagement
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Year-end sale
              </a>
            </li>
          </ul>
        </div>
      </nav>


      <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
          @yield('dashboard')
        </div>
      </main>

    </div>

</div>

<script type="text/javascript">
  $(document).ready(function () {
    $('#mytable').DataTable({
      dom: 'Bfrtip',
      buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ]
    } );
} );
</script>

  <!-- Scripts -->
  <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('js/popper.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/formbrowser.js') }}"></script>

  <script type="text/javascript">
    $(document).ready(function(){
      $('#btnHideFooter').click(function () {
        $('#footer').slideToggle();
      })

      $('#btnHideMsg').click(function () {
        $('#msg').slideToggle();
      })

      $('#btnTambahMeja').click(function () {
        $('#tambahMeja').slideToggle();
      })

      $('#warna1').click(function () {
        $('#supernav').css('background','linear-gradient(to right,#333, #FF6699)');
        $('#supernav').css('background','-webkit-linear-gradient(left,#333, #FF6699)');
        $('#supernav').css('background','-o-linear-gradient(right,#333, #FF6699)');
        $('#supernav').css('background','-moz-linear-gradient(right,#333, #FF6699)');
      })

      $('#warna2').click(function () {
        $('#supernav').css('background','linear-gradient(to right,#333, #44FFAA)');
        $('#supernav').css('background','-webkit-linear-gradient(left,#333, #44FFAA)');
        $('#supernav').css('background','-o-linear-gradient(right,#333, #44FFAA)');
        $('#supernav').css('background','-moz-linear-gradient(right,#333, #44FFAA)');
      })

      $('#warna3').click(function () {
        $('#supernav').css('background','linear-gradient(to right,#333, #6699FF)');
        $('#supernav').css('background','-webkit-linear-gradient(left,#333, #6699FF)');
        $('#supernav').css('background','-o-linear-gradient(right,#333, #6699FF)');
        $('#supernav').css('background','-moz-linear-gradient(right,#333, #6699FF)');
      })

       bsCustomFileInput.init()

    });
  </script>

</body>
</html>
