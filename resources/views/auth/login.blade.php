@extends('layouts.login')

@section('content')
<div class="container">
    <div class="row">
      <div class="col-md-8 mx-auto d-block" style="margin-top: 85px; opacity: 0.80;">
        <div class="card text-white">
          <img class="card-img" src="{{ asset('storage/discover.jpg') }}" alt="Card image" height="368">
          <div class="card-img-overlay">
            <h4 class="card-title">Nextoran</h4>
            <p class="card-text">
              For your good restaurant
            </p>
          </div>
        </div>
        </div>
        <div class="col-md-4 mx-auto d-block" style="margin-top: 85px; opacity: 0.92;">
          <form class="box-login" method="POST" action="{{ route('login') }}">
              @csrf

              <h1>Login</h1>
            <input id="username" type="text" name="username" value="{{ old('username') }}" placeholder="Username" required autofocus/>
            @if ($errors->has('username'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('username') }}</strong>
                </span>
            @endif
            <input id="password" type="password" name="password" placeholder="Password" required/>
            @if ($errors->has('password'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
            <input type="submit" value="Login"/>
            @if (Route::has('password.request'))
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
            @endif
          </form>
        </div>
    </div>
</div>
@endsection
