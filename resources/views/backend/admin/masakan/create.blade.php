@extends('layouts.admin')

@section('dashboard')

        <div class="col-md-12 col-md-offset-2">

            @if (session('msg'))
              <div id="msg" class="alert alert-danger">
                <h4>
                  <strong>{{ session('msg') }}</strong>
                  <button id="btnHideMsg" type="button" class="close" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </h4>
              </div>
            @endif
                    <form method="POST" action="/masakan" enctype="multipart/form-data">
                    <div class="form-group">
                    <label for="nama_masakan">Nama Masakan</label>
                      <input type="text" name="nama_masakan" class="form-control" value="{{ old('nama_masakan') }}" required>
                      @if ($errors->has('nama_masakan'))
                        <p class="text-danger">
                            <strong>{{ $errors->first('nama_masakan') }}</strong>
                        </p>
                      @endif
                    </div>
                    <div class="form-group">
                      <label for="harga">Harga</label>
                      <div class="row">
                        <div class="col-sm-1">
                          <span class="btn btn-light text-muted" >Rp.</span>
                        </div>
                        <div class="col">
                          <input type="number" name="harga" class="form-control border-left-0" value="{{ old('harga') }}" required>
                        </div>
                      </div>
                        @if ($errors->has('harga'))
                        <p class="text-danger">
                            <strong>{{ $errors->first('harga') }}</strong>
                        </p>
                      @endif
                    </div>
                    <div class="form-group">
                      <label for="status_masakan">Status Masakan</label>
                      <select class="custom-select" name="status_masakan">
                        <option value="1">Available</option>
                        <option value="2">Sold Out</option>
                      </select>
                    </div>
                    <label for="image">image</label>
                    <div class="custom-file" style="margin-bottom:10px;">
                      <input type="file" class="custom-file-input" id="customFile" name="image">
                      <label class="custom-file-label" for="customFile" >Choose file</label>
                    </div>
                    @if ($errors->has('image'))
                      <p class="text-danger">
                          <strong>{{ $errors->first('image') }}</strong>
                      </p>
                    @endif
                    <div class="form-group">
                    <label for="description">Description</label>
                      <input type="text" name="description" class="form-control" value="{{ old('description') }}" maxlength="70" required>
                      @if ($errors->has('description'))
                        <p class="text-danger">
                            <strong>{{ $errors->first('description') }}</strong>
                        </p>
                      @endif
                    </div>

                    {{ csrf_field() }}

                    <a href="/masakan" class="btn btn-outline-danger">
                      <i class="fas fa-times"></i> Cancel </a>
                    <button type="submit" class="btn btn-outline-success">
                      <i class="fas fa-save"></i> Submit </button>
                </form>
                </div>
              </div>
            </div>
          </div>
      </div>
@endsection
