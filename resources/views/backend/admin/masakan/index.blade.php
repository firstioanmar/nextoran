@extends('layouts.admin')

@section('dashboard')

        <div class="col-md-12 col-md-offset-2">

            @if (session('msg'))
              <div id="msg" class="alert alert-danger">
                <h4>
                  <strong>{{ session('msg') }}</strong>
                  <button id="btnHideMsg" type="button" class="close" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </h4>
              </div>
            @endif

          <a href="/masakan/create" class="btn btn-outline-success btn-sm" style="margin-bottom: 10px;">
              <i class="fas fa-plus"></i>
              Tambah data
          </a>
          <a href="" class="btn btn-outline-danger btn-sm" style="margin-bottom: 10px;" data-toggle="modal" data-target="#modalTrashed">
              <i class="fas fa-trash-alt"></i>
              Trash
          </a>

          @extends('backend.admin.masakan.trashed')

          <table id="mytable" class="table table-hover table-bordered display nowrap" style="width:100%">
            <tr>
              <th>No</th>
              <th>Masakan</th>
              <th>Harga</th>
              <th>Status masakan</th>
              <th>Image</th>
              <th>Description</th>
              <th>Option</th>
            </tr>
              @foreach ($masakan as $data)
            <tr>
              <td>{{ $no++ }}</td>
              <td>{{ $data->nama_masakan }}</td>
              <td>Rp. {{ number_format($data->harga,0,',','.') }}</td>
              <td>{{ $data->status_masakan }}</td>
              <td>
                <a href="" class="btn btn-outline-secondary btn-sm" data-toggle="modal" data-target="#modalImage{{ $data->id }}">
                    {{ $data->image }}
                </a>
              </td>
              <td>{{ $data->description }}</td>
              <td>
                <div class="btn-group" role="group" aria-label="Basic example">
                  <div>
                    <a href="/masakan/{{ $data->id }}/edit" class="btn btn-outline-primary btn-sm border-right-0">
                        <i class="fas fa-edit"></i>
                    </a>
                  </div>
                <form action="/masakan/{{ $data->id }}" method="POST">
                  <button type="submit" class="btn btn-outline-danger btn-sm border-left-0" name="submit">
                    <i class="fas fa-trash-alt"></i>
                  </button>
                  {{ csrf_field() }}
                  <input type="hidden" name="_method" value="DELETE">
                </form>
              </div>
              </td>
            </tr>
              <!-- Modal -->
              <div class="modal fade" id="modalImage{{ $data->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">{{ $data->nama_masakan }}</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <img src="{{ asset('storage/masakan/' . $data->image) }}" alt="" width="100%">
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
              @endforeach
          </table>
      </div>
@endsection
