@extends('layouts.admin')

@section('dashboard')

        <div class="col-md-12 col-md-offset-2">

            @if (session('msg'))
              <div id="msg" class="alert alert-danger">
                <h4>
                  <strong>{{ session('msg') }}</strong>
                  <button id="btnHideMsg" type="button" class="close" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </h4>
              </div>
            @endif

          <a href="" class="btn btn-outline-danger btn-sm" style="margin-bottom: 10px;" data-toggle="modal" data-target="#modalTrashed">
              <i class="fas fa-trash-alt"></i>
              Trash
          </a>

          @extends('backend.admin.order.trashed')

          <table id="table" class="table table-hover table-bordered display nowrap" style="width:100%">
            <tr>
              <th>No</th>
              <th>Nama Pelanggan</th>
              <th>Meja</th>
              <th>Status Order</th>
              <th>Option</th>
            </tr>
              @foreach ($orders as $data)
            <tr>
              <td>{{ $no++ }}</td>
              <td>{{ $data->nama_pelanggan }}</td>
              <td>{{ $data->meja->nama_meja }}</td>
              @if ($data->status_order == 1) <td>Diterima</td>
              @elseif ($data->status_order == 2) <td>Ditolak</td>
              @elseif ($data->status_order == 3)  <td>Menunggu</td>
              @endif
              <td>
                <div class="btn-group" role="group" aria-label="Basic example">
                  <div>
                    <button class="btn btn-outline-dark btn-sm border-right-0" data-toggle="modal" data-target="#modalDetail{{ $data->id }}">
                        <i class="fas fa-search"></i>
                    </button>
                  </div>
                  <div>
                    <a href="/orders/{{ $data->id }}/edit" class="btn btn-outline-primary btn-sm border-right-0 border-left-0">
                        <i class="fas fa-edit"></i>
                    </a>
                  </div>
                <form action="/orders/{{ $data->id }}" method="POST">
                  <button type="submit" class="btn btn-outline-danger btn-sm border-left-0" name="submit">
                    <i class="fas fa-trash-alt"></i>
                  </button>
                  {{ csrf_field() }}
                  <input type="hidden" name="_method" value="DELETE">
                </form>
              </div>
              </td>
            </tr>

          <!-- Modal -->
          <div class="modal fade" id="modalDetail{{ $data->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Detail Order {{ $data->nama_pelanggan }}</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <ul class="list-group">
                      <li class="list-group-item">
                        <h5 class="mb-1">User</h5>
                        @if ($data->user_id == NULL)Tidak ada
                        @else{{ $data->user->name }}@endif
                      </li>
                      <li class="list-group-item">
                        <h5 class="mb-1">Keterangan</h5>
                        {{ $data->keterangan }}</li>
                      </ul>
                </div>
              </div>
            </div>
          </div>

              @endforeach
          </table>
      </div>
@endsection
