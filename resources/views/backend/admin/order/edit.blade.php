@extends('layouts.admin')

@section('dashboard')
<div class="col-md-12">
        @if (session('msg'))
          <div id="msg" class="alert alert-success">
            <h4>
              <strong>{{ session('msg') }}</strong>
              <button id="btnHideMsg" type="button" class="close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </h4>
          </div>
        @endif

        <form method="POST" action="/orders/{{ $orders->id }}">
            @csrf

            <div class="form-group row">
                <label for="nama_pelanggan" class="col-sm-2 col-form-label">Nama Pelanggan</label>
                <div class="col-sm-8">
                    <input id="nama_pelanggan" type="text" class="form-control{{ $errors->has('nama_pelanggan') ? ' is-invalid' : '' }}" name="nama_pelanggan" value="{{ $orders->nama_pelanggan }}" required autofocus>
                    @if ($errors->has('nama_pelanggan'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('nama_pelanggan') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Meja</label>
              <div class="col-sm-8">
                <select class="custom-select" name="meja">
                  @foreach ($meja as $data)
                    <option value="{{ $data->id }}" <?php if($data->id == $orders->meja_id) echo "selected"; ?>>{{ $data->nama_meja }}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-group row">
            <label class="col-sm-2 col-form-label">Status Order</label>
              <div class="col-sm-8">
                <select class="custom-select" name="status_order">
                  <option value="1" <?php if($orders->status_order == 1) echo "selected"; ?>>Diterima</option>
                  <option value="2" <?php if($orders->status_order == 2) echo "selected"; ?>>Ditolak</option>
                  <option value="3" <?php if($orders->status_order == 3) echo "selected"; ?>>Menunggu</option>
                </select>
              </div>
            </div>

            <div class="form-group row">
                <label for="keterangan" class="col-sm-2 col-form-label">Keterangan</label>
                <div class="col-sm-8">
                  <input id="keterangan" type="text" class="form-control{{ $errors->has('keterangan') ? ' is-invalid' : '' }}" name="keterangan" value="{{ $orders->keterangan }}" required>
                  @if ($errors->has('keterangan'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('keterangan') }}</strong>
                    </span>
                  @endif
                </div>
            </div>

        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">

          <a href="/orders" class="btn btn-outline-danger">
              <i class="fas fa-times"></i> Cancel
          </a>
          <button type="submit" class="btn btn-outline-primary">
              <i class="fas fa-save"></i> Save
          </button>
        </form>
      </div>
@endsection
