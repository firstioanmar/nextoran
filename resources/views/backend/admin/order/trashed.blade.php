<!-- Modal -->
<div class="modal fade " id="modalTrashed" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Trash data Masakan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <table id="table" class="table table-hover table-bordered display nowrap" style="width:100%">
          <tr>
            <th>No</th>
            <th>Nama Pelanggan</th>
            <th>Meja</th>
            <th>Status Order</th>
            <th>Option</th>
          </tr>
            @foreach ($trashed as $data)
          <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $data->nama_pelanggan }}</td>
            <td>{{ $data->meja->nama_meja }}</td>
            @if ($data->status_order == 1) <td>Diterima</td>
            @elseif ($data->status_order == 2) <td>Ditolak</td>
            @elseif ($data->status_order == 3)  <td>Menunggu</td>
            @endif
            <td>
              <div class="btn-group" role="group" aria-label="Basic example">
                  <a href="/orders/{{ $data->id }}/restore" class="btn btn-sm btn-outline-info border-right-0">
                    <i class="fas fa-undo"></i>
                  </a>
                <a href="/orders/{{ $data->id }}/forcedelete" class="btn btn-outline-danger btn-sm border-left-0" name="submit">
                  <i class="fas fa-trash-alt"></i>
                </a>
            </div>
            </td>
          </tr>
            @endforeach
        </table>

          </div>
          @if ($trashed->count() > 0)
            <div class="modal-footer">
              <div class="col-sm-6">
                <a href="/orders/restore/all" class="btn btn-outline-primary btn-block">
                  <i class="fas fa-undo"></i> Restore All
                </a>
              </div>
              <div class="col-sm-6">
                <a href="/orders/forcedelete/all" class="btn btn-outline-danger btn-block">
                  <i class="fas fa-trash-alt"></i> Delete All
                </a>
              </div>
           </div>
          @endif
        </div>
      </div>
    </div>
