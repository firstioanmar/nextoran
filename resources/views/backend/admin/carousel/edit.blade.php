@extends('layouts.admin')

@section('dashboard')

        <div class="col-md-12 col-md-offset-2">

            @if (session('msg'))
              <div id="msg" class="alert alert-danger">
                <h4>
                  <strong>{{ session('msg') }}</strong>
                  <button id="btnHideMsg" type="button" class="close" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </h4>
              </div>
            @endif
                <form method="POST" action="/carousel/{{ $carousel->id }}" enctype="multipart/form-data">

                <div class="form-group">
                <label for="header">Header</label>
                  <input type="text" name="header" class="form-control" value="{{ $carousel->header }}" required>
                  @if ($errors->has('header'))
                    <p class="text-danger">
                        <strong>{{ $errors->first('header') }}</strong>
                    </p>
                  @endif
                </div>

                <div class="form-group">
                <label for="caption">Caption</label>
                  <input type="text" name="caption" class="form-control" value="{{ $carousel->caption }}" maxlength="30" required>
                  @if ($errors->has('caption'))
                    <p class="text-danger">
                        <strong>{{ $errors->first('caption') }}</strong>
                    </p>
                  @endif
                </div>

                <label for="image">image</label>
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="customFile" name="image">
                  <label class="custom-file-label" for="customFile" >{{ $carousel->image }}</label>
                </div>
                @if ($errors->has('image'))
                  <p class="text-danger">
                      <strong>{{ $errors->first('image') }}</strong>
                  </p>
                @endif


                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PUT">

              <button type="submit" style="margin-top:10px;" class="btn btn-outline-primary">
                <i class="fas fa-save"></i>  Save
              </button>
            </form>
            </div>
          </div>
        </div>
      </div>

      </div>
@endsection
