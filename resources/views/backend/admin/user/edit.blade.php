@extends('layouts.admin')

@section('dashboard')

        <div class="col-md-12 col-md-offset-2">

            @if (session('msg'))
              <div id="msg" class="alert alert-danger">
                <h4>
                  <strong>{{ session('msg') }}</strong>
                  <button id="btnHideMsg" type="button" class="close" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </h4>
              </div>
            @endif

          <form method="POST" action="/users/{{ $user->id }}">
          <div class="form-group">
          <label for="name">Name</label>
            <input type="text" name="name" class="form-control" value="{{ $user->name }}" autofocus>
            @if ($errors->has('name'))
              <p class="text-danger">
                  <strong>{{ $errors->first('name') }}</strong>
              </p>
            @endif
          </div>
          <div class="form-group">
          <label for="username">Username</label>
            <input type="text" name="username" class="form-control" value="{{ $user->username }}">
            @if ($errors->has('username'))
              <p class="text-danger">
                  <strong>{{ $errors->first('username') }}</strong>
              </p>
            @endif
          </div>
          <div class="form-group">
          <label for="email">Email</label>
            <input type="text" name="email" class="form-control" value="{{ $user->email }}">
            @if ($errors->has('email'))
              <p class="text-danger">
                  <strong>{{ $errors->first('email') }}</strong>
              </p>
            @endif
          </div>
          <div class="form-group">
          <label for="level_id">Level</label>
            <select class="custom-select" name="level_id">
              @foreach ($levels as $level)
                @if ($level->id > 1)
                  <option value="{{ $level->id }}" <?php if($level->id == $user->level_id) echo "selected";?>>{{ $level->nama_level }}</option>
                @endif
              @endforeach
            </select>
            @if ($errors->has('level_id'))
              <p class="text-danger">
                  <strong>{{ $errors->first('level_id') }}</strong>
              </p>
            @endif
          </div>
          <div class="form-group">
          <label for="password">New Password</label>
            <input type="text" name="password" class="form-control">
            <span class="text-muted">Isi jika ingin mengganti password</span>
            @if ($errors->has('password'))
              <p class="text-danger">
                  <strong>{{ $errors->first('password') }}</strong>
              </p>
            @endif
          </div>

          {{ csrf_field() }}
          <input type="hidden" name="_method" value="PUT">

        <button type="submit" class="btn btn-outline-success"> Submit </button>
      </form>
      </div>
@endsection
