@extends('layouts.admin')

@section('dashboard')

        <div class="col-md-12 col-md-offset-2">

            @if (session('msg'))
              <div id="msg" class="alert alert-danger">
                <h4>
                  <strong>{{ session('msg') }}</strong>
                  <button id="btnHideMsg" type="button" class="close" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </h4>
              </div>
            @endif

          <a href="/users/create" class="btn btn-outline-success btn-sm" style="margin-bottom: 10px;">
              <i class="fas fa-plus"></i>
              Tambah data
          </a>
          <a href="" class="btn btn-outline-danger btn-sm" style="margin-bottom: 10px;" data-toggle="modal" data-target="#modalTrashed">
              <i class="fas fa-trash-alt"></i>
              Trash
          </a>

          @extends('backend.admin.user.trashed')

          <table id="table" class="table table-hover table-bordered display nowrap" style="width:100%">
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Username</th>
              <th>Email</th>
              <th>Level</th>
              <th>Option</th>
            </tr>
              @foreach ($users as $data)
              <tr>
                  <td>{{ $no++ }}</td>
                  <td>{{ $data->name }}</td>
                  <td>{{ $data->username }}</td>
                  <td>{{ $data->email }}</td>
                  <td>{{ $data->level->nama_level }}</td>
                  <td>
                    @if ($data->level_id == 1)
                      <span class="text-danger"> Disable </span>
                    @else
                      <div class="btn-group" role="group" aria-label="Basic example">
                        <div>
                          <a href="/users/{{ $data->id }}/edit" class="btn btn-outline-primary btn-sm border-right-0">
                              <i class="fas fa-edit"></i>
                          </a>
                        </div>
                      <form action="/users/{{ $data->id }}" method="POST">
                        <button type="submit" class="btn btn-outline-danger btn-sm border-left-0" name="submit">
                          <i class="fas fa-trash-alt"></i>
                        </button>
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">
                      </form>
                    </div>
                    @endif
                  </td>
                </tr>
              @endforeach
          </table>
      </div>
@endsection
