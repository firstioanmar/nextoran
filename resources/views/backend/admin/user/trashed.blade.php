<!-- Modal -->
<div class="modal fade " id="modalTrashed" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Trashed data Masakan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

          <table id="table" class="table table-hover table-bordered display nowrap" style="width:100%">
            <tr>
              <th>Id</th>
              <th>Masakan</th>
              <th>Harga</th>
              <th>Description</th>
              <th>Option</th>
            </tr>
              @foreach ($trashed as $trash)
              <tr>
                  <td> <small>{{ $trash->id }}</small> </td>
                  <td><small>{{ $trash->name }}</small></td>
                  <td><small>{{ $trash->username }}</small></td>
                  <td><small>{{ $trash->email }}</small></td>
                  <td>
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <a href="/users/{{ $trash->id }}/restore" class="btn btn-sm btn-outline-info border-right-0">
                          <i class="fas fa-undo"></i>
                        </a>
                      <a href="/users/{{ $trash->id }}/forcedelete" class="btn btn-outline-danger btn-sm border-left-0" name="submit">
                        <i class="fas fa-trash-alt"></i>
                      </a>
                  </div>
                  </td>
                </tr>
              @endforeach
          </table>

          </div>
          @if ($trashed->count() > 0)
            <div class="modal-footer">
              <div class="col-sm-6">
                <a href="/users/restore/all" class="btn btn-outline-primary btn-block">
                  <i class="fas fa-undo"></i> Restore All
                </a>
              </div>
              <div class="col-sm-6">
                <a href="/users/forcedelete/all" class="btn btn-outline-danger btn-block">
                  <i class="fas fa-trash-alt"></i> Delete All
                </a>
              </div>
           </div>
          @endif
        </div>
      </div>
    </div>
