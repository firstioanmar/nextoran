@extends('layouts.app')

@section('content')

<div class="container">
  @if (session('msg'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <strong>{{ session('msg') }}</strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  @endif
    <div class="list-group" style="margin-bottom:20px;">
      @foreach ($orderku as $data)
        <a href="/orderan/{{ $data->id }}/detailorder" class="list-group-item list-group-item-action flex-column align-items-start">
          <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">{{ $data->meja->nama_meja }}</h5>
            <small>{{ $data->updated_at }}</small>
          </div>
          <p class="mb-1">{{ $data->nama_pelanggan }}</p>
          <small>Diterima oleh : {{ $data->user->name }}</small>
        </a>
      @endforeach
    </div>
  <div class="row">
    @if ($order->count() > 0)
      @foreach ($order as $data)
        <div class="col-sm-12" style="margin-bottom:20px;">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">{{ $data->meja->nama_meja }}</h4>
              <p class="card-text">{{ $data->nama_pelanggan }}</p>
                <div class="row">
                  <div class="col">
                    <a href="/orderan/{{ $data->id }}/accept" class="btn btn-outline-primary btn-block">
                      <i class="fas fa-check"></i>
                    </a>
                  </div>
                  <div class="col">
                    @if (Auth::user()->level_id <= 2 )
                      <a href="/orderan/{{ $data->id }}/cancel" class="btn btn-outline-danger btn-block">
                        <i class="fas fa-times"></i>
                      </a>
                    @else
                      <button style="cursor:not-allowed;" type="submit" class="btn btn-outline-danger btn-block" disabled>
                        <i class="fas fa-times"></i>
                      </button>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
      @endforeach
    @else
      <div class="alert alert-danger col-sm-12" role="alert">
        <strong>Belum ada Order</strong>
      </div>
    @endif
  </div>
</div>

@endsection
