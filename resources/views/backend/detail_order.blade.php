@extends('layouts.app')

@section('content')

<div class="container">
  @if (session('msg'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <strong>{{ session('msg') }}</strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  @endif
  <div class="row">
    <div class="col-md-4">
      {{-- data makanan --}}
        <div class="list-group scroll">
          <a href="#" class="list-group-item list-group-item-action flex-column align-items-start active">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">Makanan</h5>
            </div>
          </a>

          @foreach ($masakan as $data)
            <form class="" action="/add/{{ $data->id }}/masakan" method="post">
              <input type="hidden" name="order_id" value="{{ $order->id }}">
              <input type="hidden" name="masakan_id" value="{{ $data->id }}">
              {{ csrf_field() }}
              <button type="submit" class="list-group-item list-group-item-action" <?php if($data->status_masakan > 1) echo "style='cursor:not-allowed' disabled";?>>{{ $data->nama_masakan }}
                <span class="badge badge-danger badge-pill"><?php if($data->status_masakan > 1) echo "Sold Out";;?></span>
              </button>
            </form>
          @endforeach
        </div>

    </div>
    <div class="col-md-8">
      <table class="table table-bordered table-hover">
        <tr>
          <th>Nama Pelanggan</th>
          <th>Meja</th>
        </tr>
          <tr>
              <td>{{ $order->nama_pelanggan }}</td>
              <td>{{ $order->meja->nama_meja }}</td>
          </tr>
      </table>
      <form action="/detailorder" method="POST">
        <input type="hidden" name="nama_pelanggan" value="{{ $order->id }}">
        <div class="list-group" style="margin-bottom:20px;">
          @foreach ($data_detail as $data)
            <a href="" class="list-group-item list-group-item-action flex-column align-items-start">
              <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">{{ $data->masakan->nama_masakan }}</h5>
                  <small>x{{ $data->qty }}</small>
              </div>
              <small>Rp. {{ number_format($data->masakan->harga*$data->qty,0,',','.') }} </small>
            </a>
          @endforeach
        </div>
        {{ $data_detail->links() }}
        {{ csrf_field() }}
        <button class="btn btn-outline-primary btn-block" type="submit" name="button">Save</button>
      </form>
    </div>

  </div>
</div>
@endsection
