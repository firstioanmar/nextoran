@extends('layouts.welcome')

@section('content')


    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        @foreach ($carousels as $carousel)
        <li data-target="#myCarousel" data-slide-to="{{ $no++ }}"></li>
        @endforeach
      </ol>
      <div class="carousel-inner">
        @foreach ($carousels as $carousel)

          @if ($carousel->id == 1)
        <div class="carousel-item active">
          @else
        <div class="carousel-item">
        @endif

          <img class="first-slide" src="{{ asset('storage/carousel/'.$carousel->image) }}" alt="First slide">
          <div class="container">
            <div class="carousel-caption text-left">
              <h1>{{ $carousel->header }}</h1>
              <p>{{ $carousel->caption }}</p>
            </div>
          </div>
        </div>
        @endforeach
      </div>
      <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>

    <div class="container">

      @if (session('msg'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Terimakasih!</strong> {{ session('msg') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @endif

      <div class="row">
        @foreach ($masakan as $masakans)
          <div class="col">
            <div class="card" style="width: 21rem; margin-top: 10px;">
              <div class="nexto-badge">
                <span>Rp. {{ number_format($masakans->harga,0,',','.') }}</span>
              </div>
              <img class="card-img-top" src="{{ asset('storage/masakan/'.$masakans->image) }}"  alt="Card image cap" height="200">
              <div class="card-body">
                <h4 class="card-title">{{ $masakans->nama_masakan }}</h4>
                <p class="card-text">{{ $masakans->description }}
              </p>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
@endsection
