@extends('layouts.welcome')

@section('content')


<div class="kosong"></div>
  <div class="container">
    
    @if (session('msg'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <strong>Terimakasih!</strong> {{ session('msg') }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    @endif

    <div class="row">
      <table class="table table-light table-striped table-hover">
        <tr>
          <th>Masakan</th>
          <th>Harga</th>
        </tr>
          @foreach ($masakans as $masakan)
          <tr>
            <td>
            <button type="button" class="btn btn-outline-secondary border-0" data-toggle="modal" data-target="#modalMasakan{{ $masakan->id }}">
              {{ $masakan->nama_masakan }}
            </button>
          </td>
            <td>Rp. {{ number_format($masakan->harga,0,',','.') }}</td>
          </tr>

          <!-- Modal -->
          <div class="modal fade" id="modalMasakan{{ $masakan->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">{{ $masakan->nama_masakan }}</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <img src="{{ asset('storage/masakan/' . $masakan->image) }}" alt="" width="100%">
                </div>
                <div class="modal-footer">
                  {{ $masakan->description }}
                </div>
              </div>
            </div>
          </div>
          @endforeach
      </table>
      {{ $masakans->links() }}
    </div>
  </div>
@endsection
