<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailOrderTmpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_order_tmp', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('order_id')->unsigned();
          $table->integer('masakan_id')->unsigned();
          $table->integer('qty');
          $table->integer('status_detail_order');
          $table->timestamps();

          $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
          $table->foreign('masakan_id')->references('id')->on('masakan')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_order_tmp');
    }
}
