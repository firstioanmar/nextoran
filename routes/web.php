<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontendController@index');
Route::get('/menu', 'FrontendController@menu');
Route::post('/order_pelanggan', 'OrderController@order');

Route::group(['middleware' => 'admin'], function() {
  Route::get('/admin', 'AdminController@dashboard');

  Route::resource('/masakan', 'masakanController');
  Route::get('/masakan/{id}/restore','MasakanController@restore');
  Route::get('/masakan/{id}/forcedelete','MasakanController@forcedelete');
  Route::get('/masakan/restore/all','MasakanController@restoreall');
  Route::get('/masakan/forcedelete/all','MasakanController@forcedeleteall');

  Route::resource('/meja', 'MejaController');
  Route::get('/meja/{id}/restore','MejaController@restore');
  Route::get('/meja/{id}/forcedelete','MejaController@forcedelete');

  Route::resource('/users', 'UserController');
  Route::get('/users/{id}/restore','UserController@restore');
  Route::get('/users/{id}/forcedelete','UserController@forcedelete');
  Route::get('/users/restore/all','UserController@restoreall');
  Route::get('/users/forcedelete/all','UserController@forcedeleteall');

  Route::resource('/level', 'LevelController');
  Route::resource('/carousel', 'CarouselController');

  Route::resource('/orders', 'OrderController');
  Route::get('/orders/{id}/restore','OrderController@restore');
  Route::get('/orders/{id}/forcedelete','OrderController@forcedelete');
  Route::get('/orders/restore/all','OrderController@restoreall');
  Route::get('/orders/forcedelete/all','OrderController@forcedeleteall');

});

Route::group(['middleware' => 'kasir'], function() {
  Route::get('/home', 'HomeController@index');
});

  Route::get('/orderan', 'HomeController@orderan');
  Route::get('/orderan/{id}/accept', 'HomeController@acceptorder');
  Route::get('/orderan/{id}/cancel', 'HomeController@cancelorder');
  Route::get('/orderan/{id}/detailorder', 'HomeController@detailorder');
  Route::post('/add/{id}/masakan', 'HomeController@addmasakan');
  Route::get('/delete/{id}/masakan', 'HomeController@deletemasakan');
  Route::post('/detailorder', 'HomeController@createdetailorder');

Auth::routes();
