<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{

  use SoftDeletes;
  protected $dates = ['deleted_at'];
  
  protected $fileable = [
    'nama_pelanggan','meja_id','keterangan','status_order'
  ];

  public function meja()
  {
    return $this->belongsTo('App\Models\Meja');
  }

  public function user()
  {
    return $this->belongsTo('App\Models\User');
  }
}
