<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailOrder extends Model
{

  protected $fileable = [
    'masakan_id','qty','order_id','keterangan','status_detail_order'
  ];

  public function order()
  {
    return $this->belongsTo('App\Models\Order');
  }

  public function masakan()
  {
    return $this->belongsTo('App\Models\Masakan');
  }
}
