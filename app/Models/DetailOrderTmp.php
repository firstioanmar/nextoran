<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailOrderTmp extends Model
{

  protected $fileable = [
    'masakan_id','qty','order_id'
  ];

  public function order()
  {
    return $this->belongsTo('App\Models\Order');
  }

  public function masakan()
  {
    return $this->belongsTo('App\Models\Masakan');
  }
}
