<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Masakan;
use App\Models\Order;
use App\Models\DetailOrderTmp;

use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $masakan = Masakan::all();
        $order = Order::where('status_order','3')->orderBy('created_at','asc')->get();
        return view('backend.home', compact('masakan','order'));
    }

    public function orderan()
    {
      $order = Order::where('status_order','3')->where('user_id',NULL)->orderBy('created_at','asc')->get();
      $orderku = Order::where('user_id',Auth::user()->id)->get();
      return view('backend.orderan', compact('order','orderku'));
    }

    public function acceptorder($id)
    {
      $order = Order::findOrFail($id);
      if (!empty($order->user_id)) {
        return redirect('/orderan')->with('msg','Maaf Orderan telah di terima oleh '.$order->user->name.' !');
      }
      $order->user_id = Auth::user()->id;
      $order->status_order = 1;
      $order->save();

      return redirect('/orderan/'.$order->id.'/detailorder');
    }

    public function cancelorder($id)
    {
      $order = Order::findOrFail($id);
      if (!empty($order->user_id)) {
        return redirect('/orderan')->with('msg','Maaf Orderan telah di terima oleh '.$order->user->name.' !');
      }
      $order->user_id = Auth::user()->id;
      $order->status_order = 2;
      $order->save();

      return redirect('/orderan');
    }

    public function detailorder($id)
    {
        $order = Order::findOrFail($id);
        $masakan = Masakan::all();
        $data_detail = DetailOrderTmp::where('order_id',$order->id)->paginate(4);

        if ($order->user_id != Auth::user()->id ) {
          return redirect('/orderan')->with('msg','Maaf Orderan telah di terima oleh '.$order->user->name.' ! silahkan hubungi kasir');
        }

        return view('backend.detail_order',compact('order','masakan','data_detail'));
    }

    public function addmasakan(Request $request)
    {
      $masakan = Masakan::findOrFail($request->masakan_id);
      $order = Order::findOrFail($request->order_id);

      if ($masakan->status_masakan > 1 ) {
        return redirect('/orderan/'.$order->id.'/detailorder')->with('msg','masakan telah habis');
      }

      $data_detail = DetailOrderTmp::where('order_id',$order->id)->first();
      $data_detail_masakan = DetailOrderTmp::where('masakan_id',$masakan->id)->first();

      if (!empty($data_detail_masakan->masakan_id)) {
        $data_detail_masakan->qty = $data_detail_masakan->qty + 1;
        $data_detail_masakan->save();
        return redirect('/orderan/'.$order->id.'/detailorder');
      }

      $detail_order_tmp = new DetailOrderTmp;
      $detail_order_tmp->masakan_id = $request->masakan_id;
      $detail_order_tmp->order_id = $request->order_id;
      $detail_order_tmp->qty = 1;
      $detail_order_tmp->status_detail_order = 1;
      $detail_order_tmp->save();

      return redirect('/orderan/'.$order->id.'/detailorder');
    }

    public function deletemasakan(Request $request,$id)
    {
      $order = Order::findOrFail($request->order_id);
      $detail_order_tmp = findOrFail($id);
      $detail_order_tmp->delete();

      return redirect('/orderan/'.$order->id.'/detailorder');
    }
}
