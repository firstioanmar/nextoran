<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Meja;
use App\Models\Order;
use Auth;

class OrderController extends Controller
{

    public function order(Request $request)
    {
      $this->middleware('auth');

      $request->validate([
        'nama' => 'required|string|max:50'
      ]);

      $insert = new Order;
      $insert->nama_pelanggan = $request->nama;
      $insert->meja_id = $request->id_meja;
      $insert->keterangan = 'keterangan';
      $insert->status_order = 3;

      $insert->save();

      return redirect('/menu')->with('msg',' silahkan tunggu waiters');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();
        $no     = 1;
        $trashed = Order::onlyTrashed()->get();

        return view('backend.admin.order.index',compact('orders','no','trashed'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.order.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $orders = Order::findOrFail($id);
        $meja   = Meja::all();
        return view('backend.admin.order.edit',compact('orders','meja'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
          'nama_pelanggan' => 'required|min:3|max:50',
          'keterangan' => 'required|max:50'
        ]);

        $order = Order::findOrFail($id);
        $order->nama_pelanggan = $request->nama_pelanggan;
        $order->meja_id = $request->meja;
        $order->status_order = $request->status_order;
        $order->keterangan = $request->keterangan;
        if ($request->status_order == 3) {
          $order->user_id = NULL;
        } else {
          $order->user_id = Auth::user()->id;
        }

        $order->save();

        return redirect('/orders/'.$order->id.'/edit')->with('msg','Data berhasil edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $data = Order::findOrFail($id);
      $data->delete();

      return redirect('/orders');
    }

    public function restore($id)
    {
      $data = Order::withTrashed()->findOrFail($id);
      $data->restore();

      return redirect('/orders');
    }

    public function forcedelete($id)
    {
      $data = Order::withTrashed()->findOrFail($id);
      $data->forceDelete();

      return redirect('/orders');
    }

    public function restoreall()
    {

      $data = Order::withTrashed();
      $data->restore();

      return redirect('/orders');
    }

    public function forcedeleteall()
    {
      $data = Order::onlyTrashed()->get();
      $data->forceDelete();

      return redirect('/orders');
    }
}
